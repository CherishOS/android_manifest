Credits:
=======
 * [**AOSP**](https://android.googlesource.com)
 * [**LineageOS**](https://github.com/LineageOS)
 * [**Nitrogen-Project**](https://github.com/nitrogen-project)
 * [**SuperiorOS**](https://github.com/SuperiorOS)
 * [**DotOS**](https://github.com/DotOS)
 * [**PixelExperience**](https://github.com/PixelExperience)
 * [**AospExtended**](https://github.com/AospExtended)
 * [**Syberia OS**](https://github.com/syberia-project)
 * [**Pixys OS**](https://github.com/PixysOS)
 * [**ArrowOS**](https://github.com/ArrowOS)
 * [**MSM-Xtended**](https://github.com/Project-Xtended)
 * [**HavocOS**](https://github.com/Havoc-OS)
 * [**BootleggersROM**](https://github.com/BootleggersROM)
 * [**ABC**](https://github.com/ezio84?tab=repositories)
 * [**Rebellion**](https://github.com/Rebellion-hub)
 * [**Komodo**](https://github.com/komodo-os-rom)
 * [**Durex aka Corvus**](https://github.com/corvus-os)
 * [**DerpFest**](https://github.com/derplab)
 * [**Evolution-X**](https://github.com/evolution-x)
 * [**OMNIROM**](https://github.com/omnirom)
 * [**AOSIP**](https://github.com/aosip)
 * [**Liquid Remix**](https://github.com/liquidremix)
 * [**Resurrection Remix**](https://github.com/ResurrectionRemix)
 * [**Dirty Unicorns**](https://github.com/DirtyUnicorns)
 * [**Bliss**](https://github.com/blissroms)
 * [**ExtendedUI**](https://github.com/extended-ui)
 * [**AICP**](https://github.com/aicp)
 * [**MoKee**](https://github.com/Mokee)
 * [**RevengeOS**](https://github.com/revengeos)
 * [**Yodita**](https://gitlab.com/yodita)
 * [**POSP**](https://github.com/PotatoProject)
 * [**ION**](https://github.com/i-o-n)
 * [**crDroid**](https://github.com/crdroidandroid)
 * [**GZOSP**](https://github.com/GZOSP)
 * [**Colt-Enigma**](https://github.com/Colt-Enigma)
 * [**AnicentROM**](https://github.com/Ancient-Lab)
 * [**ExtendedUI**](https://github.com/Extended-UI)
----------------------------------------------------------------------------

Getting Started:
==============

To get started with the building process, you'll need to get familiar with [Git and Repo](http://source.android.com/source/using-repo.html).

Install the build packages:
===============

Tested on Ubuntu 16.04,16.10,17.04,18.04,18.10,19.04:

```bash
 sudo apt install bc bison build-essential ccache curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick lib32ncurses5-dev lib32readline-dev lib32z1-dev liblz4-tool libncurses5-dev libsdl1.2-dev libssl-dev libwxgtk3.0-dev libxml2 libxml2-utils lzop pngcrush rsync schedtool squashfs-tools xsltproc zip zlib1g-dev
```

To initialize your local repository, use a command like this:

```bash
    repo init -u https://gitlab.com/CherishOS/android_manifest.git -b ten 
```

Then to sync up:
================

```bash
    repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --no-tags
```
Compilation of Cherish OS:
====================

From root directory of Project, perform following commands in terminal


```bash
. build/envsetup.sh
 brunch device-codename
```
-----------------------------------------------------------------------------
